
/**
 * PicturePanel.java
 * Author: Jori Gelbaugh
 * Date: January 24, 2017
 * Version: 2.0
 *
 *Creates a dog that starves if its food runs out or dies from obesity if it is fed too much.
 *The treat increases its food more rapidly than the healthy dog food, and the ball will decrease
 *its food level.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Creates a PicturePanel class that controls the dog.
 */
@SuppressWarnings("serial")
public class PicturePanel extends JPanel {
	// Keeps track of the amount of food in the dog
	int foodLevel;
	// Keeps track of whether or not the dog is alive
	boolean isAlive;
	// Creates an ArrayList that's populated with circles
	ArrayList<Circle> circles;

	/**
	 * Constructor for PicturePanel class
	 */
	public PicturePanel() {
		// Handles mouse events
		MouseHandler mh = new MouseHandler();
		addMouseListener(mh);
		addMouseMotionListener(mh);

		// Initialize foodLevel to 5.
		foodLevel = 5;
		// Sets the dog to living
		isAlive = true;
		// Creates an ArrayList of circles to be drawn when the dog dies
		circles = new ArrayList<Circle>();
	}

	/**
	 * Called when the aplet needs to be drawn and paints the dog
	 */
	public void paintComponent(Graphics g) {
		// Initializes paintComponent
		super.paintComponent(g);

		// draws frame
		g.drawRect(0, 0, 600, 600);

		// name of author in lower right
		g.setColor(Color.black);
		g.setFont(new Font("Arial", Font.BOLD, 10));
		g.drawString("Author: Jori Gelbaugh", 485, 565);

		// Implements and draws the dog
		isHealthy(g);
		drawEyes(g);
		drawNose(g);
		drawMouth(g);
		drawEars(g);
		drawTag(g);
		showFoodLevel(g);
		drawTreat(g);
		drawBall(g);
		drawFood(g);

		// Calls the isDead method if the dog is starving or obese
		if (foodLevel < 0 || foodLevel > 15) {
			isDead(g);
			isAlive = false;
		}
	}

	/**
	 * Paints the food level of the dog.
	 * 
	 * @param g
	 */
	private void showFoodLevel(Graphics g) {
		g.setColor(Color.black);
		g.setFont(new Font("Times New Roman", Font.BOLD, 30));
		g.drawString("Food Level: " + foodLevel, 223, 125);
	}

	/**
	 * Paints the tag of the dog.
	 * 
	 * @param g
	 */
	private void drawTag(Graphics g) {
		// draws a yellow name tag
		g.setColor(Color.yellow);
		g.fillOval(280, 470, 65, 65);
		g.setColor(Color.black);
		g.drawOval(280, 470, 65, 65);

		// Draw a string informing the user about the dog
		g.setColor(Color.black);
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("Cody", 290, 512);
	}

	/**
	 * Paints the mouth of the dog.
	 * 
	 * @param g
	 */
	private void drawMouth(Graphics g) {
		// draws mouth
		g.setColor(Color.black);
		g.drawLine(220, 375, 375, 375);
	}

	/**
	 * Paints the nose of the dog.
	 * 
	 * @param g
	 */
	private void drawNose(Graphics g) {
		// draws circle of nose
		g.setColor(Color.black);
		g.fillOval(275, 275, 60, 60);

		// draws line down from nose
		g.setColor(Color.black);
		g.drawLine(300, 300, 300, 375);

	}

	/**
	 * Paints the ears of the dog.
	 * 
	 * @param g
	 */
	private void drawEars(Graphics g) {
		// draws left ear
		g.setColor(new Color(102, 51, 0));
		g.fillRoundRect(110, 115, 90, 190, 90, 90);

		// draws right ear
		g.setColor(new Color(102, 51, 0));
		g.fillRoundRect(395, 115, 90, 190, 90, 90);

		// fill left ear
		g.setColor(Color.black);
		g.fillRoundRect(128, 150, 50, 110, 90, 90);

		// fill right ear
		g.setColor(Color.black);
		g.fillRoundRect(415, 150, 50, 110, 90, 90);
	}

	/**
	 * Paints the eyes of the dog.
	 * 
	 * @param g
	 */
	private void drawEyes(Graphics g) {
		// creates the right eye of the dog
		g.setColor(Color.white);
		g.fillOval(325, 200, 60, 60);

		// creates the left eye of the dog
		g.setColor(Color.white);
		g.fillOval(225, 200, 60, 60);

		// creates the right iris of the dog
		g.setColor(Color.black);
		g.fillOval(325, 215, 30, 30);

		// creates the left iris of the dog
		g.setColor(Color.black);
		g.fillOval(225, 215, 30, 30);
	}

	/**
	 * Determines if the dog is healthy based on foodLevel.
	 * 
	 * @param g
	 */
	private void isHealthy(Graphics g) {
		// If the dog has starved to death, set it to starving
		if (foodLevel < 2) {
			setStarving(g);
		}
		// Draws a "normal"/healthy face if the dog has the right amount of food
		else if (foodLevel <= 10) {
			// draws face
			g.setColor(new Color(102, 51, 0));
			g.fillOval(140, 145, 325, 325);
			// draws right whisker
			g.setColor(Color.black);
			g.drawLine(375, 325, 375, 375);

			// draws left whisker
			g.setColor(Color.black);
			g.drawLine(220, 375, 220, 325);

			// draws tongue
			g.setColor(Color.red);
			g.fillArc(280, 351, 35, 50, 180, 180);
			g.setColor(Color.black);
			g.drawArc(280, 351, 35, 50, 180, 180);
		}
		// Draws an "overweight" face if the dog needs some exercise
		else if (foodLevel > 10) {
			// draws face
			g.setColor(new Color(102, 51, 0));
			g.fillOval(140, 145, 375, 375);

			// draws right whisker
			g.setColor(Color.black);
			g.drawLine(375, 375, 375, 400);

			// draws left whisker
			g.setColor(Color.black);
			g.drawLine(220, 375, 220, 400);

			// draws tongue
			g.setColor(Color.red);
			g.fillArc(280, 351, 35, 50, 180, 180);
			g.setColor(Color.black);
			g.drawArc(280, 351, 35, 50, 180, 180);
		}
	}

	/**
	 * Draws what the dog looks like if it's starving.
	 * 
	 * @param g
	 */
	private void setStarving(Graphics g) {
		// draws face
		g.setColor(new Color(102, 51, 0));
		g.fillOval(190, 145, 225, 275);

		// draws right whisker
		g.setColor(Color.black);
		g.drawLine(375, 375, 375, 385);

		// draws left whisker
		g.setColor(Color.black);
		g.drawLine(220, 375, 220, 385);
	}

	/**
	 * Draws the dog treat.
	 * 
	 * @param g
	 */
	private void drawTreat(Graphics g) {
		// draws left side
		g.setColor(new Color(193, 168, 97));
		g.fillOval(25, 25, 25, 25);
		g.setColor(new Color(193, 168, 97));
		g.fillOval(25, 45, 25, 25);

		// draws right side
		g.setColor(new Color(193, 168, 97));
		g.fillOval(100, 25, 25, 25);
		g.setColor(new Color(193, 168, 97));
		g.fillOval(100, 45, 25, 25);

		// draws center
		g.setColor(new Color(193, 168, 97));
		g.fillRect(31, 35, 75, 20);
		g.setColor(Color.black);
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("Treat", 50, 50);
	}

	/**
	 * Draws the ball for the dog.
	 * 
	 * @param g
	 */
	private void drawBall(Graphics g) {
		// Draws the ball of the dog and labels it
		g.setColor(new Color(102, 255, 102));
		g.fillOval(275, 15, 75, 75);
		g.setColor(Color.black);
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("Ball", 295, 60);
	}

	/**
	 * Draws the food for the dog.
	 * 
	 * @param g
	 */
	private void drawFood(Graphics g) {
		// draws bowl
		g.setColor(Color.red);
		g.fillRoundRect(450, 25, 100, 50, 180, 180);
		g.setColor(Color.black);
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("Food", 475, 70);

		// draws food
		g.setColor(new Color(102, 51, 0));
		g.fillArc(462, 18, 75, 50, 180, -180);
	}

	/**
	 * Dictates what to do if the dog dies from starvation or obesity.
	 * 
	 * @param g
	 */
	private void isDead(Graphics g) {
		// Initializes paintComponent
		super.paintComponent(g);

		// draws frame
		g.drawRect(0, 0, 600, 600);

		// Tells user that they've killed Cody
		g.setFont(new Font("Times New Roman", Font.BOLD, 20));
		g.drawString("You killed Cody!", 300, 300);

		// Populates the frame with circles
		for (Circle c : circles) {
			g.setColor(Color.lightGray);
			g.fillOval(c.getX(), c.getY(), c.getWidth(), c.getHeight());
		}
	}

	// ------------------------------------------------------------------------------------------

	// ---------------------------------------------------------------
	// A class to handle the mouse events for the applet.
	// This is one of several ways of handling mouse events.
	// If you do not want/need to handle mouse events, delete the following
	// code.
	//
	private class MouseHandler extends MouseAdapter implements MouseMotionListener {

		/**
		 * Creates boolean fields to determine what's been pressed for drag
		 * function.
		 */
		private boolean foodPressed;
		private boolean ballPressed;
		private boolean treatPressed;

		/**
		 * Determine what happens if a food/ball item is clicked.
		 */
		public void mouseClicked(MouseEvent e) {
			// instantiates the number of clicks
			int numberClicks = 0;
			// Gets the x and y coordinates
			int xPoint = e.getX();
			int yPoint = e.getY();
			// Determines what happens if the food is clicked
			if ((xPoint > 449 && xPoint < 551) && (yPoint > 19 && yPoint < 74)) {
				foodLevel++;
				numberClicks++;
			}
			// Determines what happens if the ball is clicked
			if ((xPoint >= 275 && xPoint <= 350) && (yPoint >= 15 && yPoint <= 90)) {
				foodLevel--;
				numberClicks++;
			}
			// Determines what happens if the treat is clicked
			if ((xPoint >= 30 && xPoint <= 120) && (yPoint >= 25 && yPoint <= 70)) {
				foodLevel += 2;
				numberClicks++;
			}
			// Populates the ArrayList of circles, depending on how often things
			// have been clicked
			int x = 10;
			int y = 10;
			if (numberClicks == 0) {
				circles.add(new Circle(x, y, 25, 25));
			}
			if (numberClicks == 1) {
				circles.add(new Circle(x + 1, y + 1, 50, 50));
			}
			if (numberClicks == 2) {
				circles.add(new Circle(x + 2, y + 2, 25, 25));
			}
			if (numberClicks == 3) {
				circles.add(new Circle(x + 15, y + 15, 10, 10));
			}
			if (numberClicks == 4) {
				circles.add(new Circle(x + 25, y + 25, 22, 22));
			}
			if (numberClicks == 5) {
				circles.add(new Circle(x + 3, y + 3, 15, 15));
			}
			repaint();
		}

		/**
		 * Determine what happens if the mouse is pressed
		 */
		public void mousePressed(MouseEvent e) {
			// Gets the x and y coordinates
			int xPoint = e.getX();
			int yPoint = e.getY();
			// Sets foodPressed to true if food is clicked
			if ((xPoint > 449 && xPoint < 551) && (yPoint > 19 && yPoint < 74)) {
				foodPressed = true;
				repaint();
			}
			// Sets ballPressed to true if ball is clicked
			else if ((xPoint >= 275 && xPoint <= 350) && (yPoint >= 15 && yPoint <= 90)) {
				ballPressed = true;
				repaint();
			}
			// Sets treatPressed to true if treat is clicked
			else if ((xPoint >= 30 && xPoint <= 120) && (yPoint >= 25 && yPoint <= 70)) {
				treatPressed = true;
				repaint();
			}
		}

		/**
		 * Determines the actions that occur is the mouse is released by the
		 * dog's mouth.
		 */
		public void mouseReleased(MouseEvent e) {
			int xPoint = e.getX();
			int yPoint = e.getY();
			// draws if the food was pressed
			if (foodPressed == true && (xPoint > 220 && xPoint < 400) && (yPoint > 275 && yPoint < 375)) {
				foodLevel++;
			}
			// draws if the ball was pressed
			else if (ballPressed == true && (xPoint >= 220 && xPoint <= 400) && (yPoint >= 275 && yPoint <= 400)) {
				foodLevel--;
			}
			// draws if the treat was pressed
			else if (treatPressed == true && (xPoint >= 220 && xPoint <= 400) && (yPoint >= 275 && yPoint <= 375)) {
				foodLevel += 2;
			}
			repaint();
		}

	}
	// End of MouseHandler class

} // Keep this line--it is the end of the PicturePanel class