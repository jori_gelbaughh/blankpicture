My Self-evaluation:
-----------------------
Correctness: 	 9/10 
Style:   	     5/6
Documentation    5/6
Learning Gains: 8/8
-----------------------
Total:			27/30
-----------------------
Hours spent: 10
-----------------------
Justification: All of the components are correct and the elements are all there, except for the nested loops.
Therefore, I gave myself a 9/10 on correctness.
I tried to document and comment as well as I could with elegant styling, but there may be some errors.
Therefore, I took one point off the Style and Documentation categories.
I learned a lot while completing this project.
Therefore, I gave myself an 8/8 on learning gains.
