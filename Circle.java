/**
 * Circle.java Author: Jori Gelbaugh Date: January 24, 2017 Version: 2.0
 *
 * Creates circles that will populate the screen when the dog dies.
 *
 */
public class Circle {
	// Creates an x coordinate field
	int xCoord;
	// Creates a y coordinate field
	int yCoord;
	// Creates a height field
	int height;
	// Creates a width field
	int width;

	/**
	 * Creates an instance of a circle
	 * 
	 * @param x,
	 *            the xCoordinate
	 * @param y,
	 *            the yCoordinate
	 * @param h,
	 *            the height
	 * @param w,
	 *            the width
	 */
	public Circle(int x, int y, int h, int w) {
		xCoord = x;
		yCoord = y;
		height = h;
		width = w;
	}

	/**
	 * Gets the x coordinate
	 * 
	 * @return xCoord
	 */
	public int getX() {
		return xCoord;
	}

	/**
	 * Gets the y coordinate
	 * 
	 * @return yCoord
	 */
	public int getY() {
		return yCoord;
	}

	/**
	 * Gets the height.
	 * 
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Gets the width
	 * 
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
}
